<!DOCTYPE html>
<html lang="ru">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="icon" href="img/favicon.ico">
<title>Тест</title>
<meta name="description" content=""/>
<link rel="canonical" href="" />
<meta property="og:locale" content="ru_RU" />
<meta property="og:type" content="website" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:url" content="" />
<meta property="og:site_name" content="" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="" />
<meta name="twitter:title" content="" />

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/animate.min.css">
<link rel="stylesheet" href="css/style.css">

</head>
<body>

<!-- section1-->


<section id="sec1">
<div class="container">
  <h2>Обработать файл</h2>
    <form enctype="multipart/form-data" action = "/" method = "post">
      <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
      Отправить этот файл: <br>
      <input name="userfile" type="file" /><br>
      <input type="submit" value="Обработать" />
    </form>
</div>
</section>

<section id="sectable">
<div class="container">
<?php
require 'class.php';

function PrintFile(FileReadFactory\FilePrint $file)
{
    $filterFields = ['login', 'firstname', 'lastname', 'email'];
    $file->setFileData($filterFields);
    $file->print();
}

if(file_exists($_FILES['userfile']['tmp_name']) || is_uploaded_file($_FILES['userfile']['tmp_name'])){

  $uploaddir = '/home/megaitby/test2.megait.by/files/';
  $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

  if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile))
  {
      echo "Файл был успешно загружен.\n";
      //echo mime_content_type($uploadfile);
      if(mime_content_type($uploadfile) === 'text/plain')
      {
          PrintFile(new FileReadFactory\CsvPrint($uploadfile));
      }
      elseif(mime_content_type($uploadfile) === 'application/json')
      {
          PrintFile(new FileReadFactory\JsonPrint($uploadfile));
      }
      elseif(mime_content_type($uploadfile) === 'text/xml')
      {
          PrintFile(new FileReadFactory\XmlPrint($uploadfile));
      }
      else
      {
          echo "Ошибка Неподходящий тип файла!\n";
      }

  }
  else
  {
      echo "Ошибка загрузки!\n";
  }
}
?>
</div>
</section>



<!-- footer
================================================== -->
<footer>
	<div class="container">


	</div>
</footer>


<!-- java script
================================================== -->
<script src="js/jquery-3.4.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript" charset="utf-8"></script>


</body>
</html>
