<?php

namespace FileReadFactory;

/**
 * Factory
 */
abstract class FilePrint
{
    protected $fileData;

    abstract public function getFile(): FileTypeReader;

    public function setFileData(array $filterFields): void
    {
        $file = $this->getFile();
        $file->read();
        $file->prepare();
        $this->fileData = $file->get($filterFields);
    }

    public function getFileData(array $filterFields): array
    {
        return $this->fileData;
    }

    public function print(): void
    {
        $output = "<table class='table'>";
        foreach($this->fileData as $row )
        {
            $output .= "<tr><td>";
            $output .= implode("</td><td>", $row);
            $output .= "</td></tr>";
        }
        $output .= "</table>";
        echo $output;
    }

}

/**
 * Constructor
 */

class JsonPrint extends FilePrint
{
    private $file;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function getFile(): FileTypeReader
    {
        return new JsonReader($this->file);
    }
}

class CsvPrint extends FilePrint
{
    private $file;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function getFile(): FileTypeReader
    {
        return new CsvReader($this->file);
    }
}

class XmlPrint extends FilePrint
{
    private $file;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function getFile(): FileTypeReader
    {
        return new XmlReader($this->file);
    }
}


/**
 * Helper Traits
 */
trait RemoveEmptyLines {
    public function removeEmptyLines($arr)
    {
        return array_values(array_filter($arr, fn($row) =>  !empty(array_filter($row))));
    }
}

trait Filter {
    public function filter($arr, $filterFields)
    {
        $indexLine = $arr[0];
        $neededKeys = Array();
        $finalArr = Array();
        foreach($filterFields as $field)
        {
            $temp = array_keys($indexLine, $field);
            if(!empty($temp)){
                $neededKeys = array_merge ($neededKeys, $temp);
            }
        }
        foreach($arr as $num => $row)
        {
            foreach($row as $elnum => $el)
            {
                if(in_array($elnum, $neededKeys)){
                    $finalArr[$num][$elnum] = $el;
                }
            }
        }
        return $finalArr;
    }
}

trait ConvertJsonArray {
    public function convertJsonArray($arr)
    {
        $tempArr = Array();
        foreach($arr as $num => $row )
        {
            if($num == 0){
                foreach($row as $name => $cell){
                    $tempArr[0][] = $name;
                }
            }
            $tempArr[] = array_values((array) $row);
        }
        return $tempArr;
    }
}

interface FileTypeReader
{
/**
* Read file to array
*/
    public function read(): void;

/**
* Normalaize output array Bring it to the [0] => field names then output data
* No empty rows
*/
    public function prepare(): void;

/**
* Filter needed fields by F name before output
*/
    public function get($filterFields): array;
}

class JsonReader implements FileTypeReader
{
    private $file, $fileData;
    use RemoveEmptyLines, ConvertJsonArray, Filter;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function read(): void
    {
        $json = file_get_contents($this->file);
        $this->fileData = json_decode($json);
    }

    public function prepare(): void
    {
        $this->fileData = $this->convertJsonArray($this->fileData);
        $this->fileData = $this->removeEmptyLines($this->fileData);
    }

    public function get($filterFields): array
    {
        $this->fileData = $this->filter($this->fileData, $filterFields);
        return $this->fileData;
    }
}

class CsvReader implements FileTypeReader
{
    private $file, $fileData;
    use RemoveEmptyLines, Filter;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function read(): void
    {
        $this->fileData = array_map('str_getcsv', file($this->file));
    }

    public function prepare(): void
    {
        $this->fileData = $this->removeEmptyLines($this->fileData);
    }

    public function get($filterFields): array
    {
        $this->fileData = $this->filter($this->fileData, $filterFields);
        return $this->fileData;
    }
}

class XmlReader implements FileTypeReader
{
    private $file, $fileData;
    use RemoveEmptyLines, ConvertJsonArray, Filter;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function read(): void
    {
        $xmlstring = file_get_contents($this->file);
        //$this->fileData = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
        $this->fileData = json_decode(json_encode((array) simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA)), true);
    }

    public function prepare(): void
    {
        $tempArr = Array();
        foreach($this->fileData['Record'] as $num => $row)
        {
            $tempArr[$num] = array_values($row['Row']['@attributes']);

        }
        $this->fileData = $tempArr;
        //print_r($this->fileData);
        //$this->fileData = $this->convertJsonArray($this->fileData);
        $this->fileData = $this->removeEmptyLines($this->fileData);
    }

    public function get($filterFields): array
    {
        $this->fileData = $this->filter($this->fileData, $filterFields);
        return $this->fileData;
    }
}
